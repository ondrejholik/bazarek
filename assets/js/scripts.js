const d = new Date();
const den = d.getDay();
const hodiny = d.getHours();
const min = d.getMinutes();
const tabulka = document.querySelector("table");
const dite = tabulka.lastElementChild.children[den - 1];

const hodmin = hodiny + (min / 60);
let stav = "";
let stav_barva = "";
if (den < 6 && den != 0) {
    dite.className = "zelena";
    if (hodiny > 11 && hodmin < 16.5) {
        stav = "Právě otevřeno!"
        stav_barva = "green";
    } else {
        stav = "Zavřeno";
        dite.className = "cervena";
        stav_barva = "red";
    }

} else { stav = "Dnes zavřeno"; }

document.getElementById("stav").innerHTML = stav;
document.getElementById("stav").style.color = stav_barva;


const em = "holikovaanna";
const ai = "@sezn"
const l = "am.cz";
document.getElementById("mail").innerHTML = em + ai + l;

let toAirtable = (email, datum, zprava) => {
    var Airtable = require('airtable');
    var base = new Airtable({ apiKey: 'keyMS9F4HPMhlwQur' }).base('appDy2FYMtaaWzTH5');
    base('zakaznici').create({
        "email": email,
        "datum": datum,
        "text": zprava
    });
    delFunc();
    alert("Úspěšně odesláno :)")
}


function emailV(email) {
    if (mailCheck(email)) {
        uspech();
    } else {
        neuspech();
    }


}
let delFunc = () => {
    document.getElementById("email").value = "";
    document.getElementById("zprava").value = "";
}

let mailCheck = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
let areaCheck = () => {

    if (area.value.length > 10) {
        return true;

    } else { return false; }
}
let email = document.getElementById("email");
let area = document.getElementById("zprava");
let help = document.getElementsByClassName("help")[0];
let right_icon = document.getElementById("right-icon");
const uspech = () => {
    email.className = "input is-success";
    help.className = "help is-success"
    help.textContent = "Email je v pořádku, můžete pokračovat :-)";
    right_icon.className = "fa fa-check";
}
const neuspech = () => {
    email.className = "input is-danger";
    help.className = "help is-danger"
    help.textContent = "Nějaká chybka v mailu ";
    right_icon.className = "fa fa-warning";


}
const odeslat = () => {
    let e_mail = document.getElementById("email");


    let check = ((mailCheck(e_mail.value)) && (areaCheck()));

    if (check) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = mm + '/' + dd + '/' + yyyy;



        toAirtable(email.value, today, area.value);


    } else {
        alert("Ujistěte se že je všechno správně vyplněné :)");
    }
}